/*
 Navicat Premium Data Transfer

 Source Server         : local_db
 Source Server Type    : PostgreSQL
 Source Server Version : 140004
 Source Host           : localhost:5432
 Source Catalog        : Simulasi_Test_BTPN
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140004
 File Encoding         : 65001

 Date: 22/08/2023 15:20:46
*/


-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS "public"."employee";
CREATE TABLE "public"."employee" (
  "nik" int4,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "address" varchar(255) COLLATE "pg_catalog"."default",
  "departement" varchar(255) COLLATE "pg_catalog"."default",
  "id" uuid NOT NULL
)
;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO "public"."employee" VALUES (32121332, 'John', 'Jl. Yang Lurus', 'IT', '62d47105-c24c-4c45-b765-005bd9cb31e6');
INSERT INTO "public"."employee" VALUES (1011, 'Christopher', 'Jl. Yang Lurus', 'HR', '97e27840-fb47-4540-bad5-017e95b67f4f');

-- ----------------------------
-- Primary Key structure for table employee
-- ----------------------------
ALTER TABLE "public"."employee" ADD CONSTRAINT "Employee_pkey" PRIMARY KEY ("id");
