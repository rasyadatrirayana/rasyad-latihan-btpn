package com.Rasyad.EmployeeCrud;

import com.Rasyad.EmployeeCrud.controllers.EmployeeController;
import com.Rasyad.EmployeeCrud.dto.request.CreateEmployeeRequest;
import com.Rasyad.EmployeeCrud.models.Employee;
import com.Rasyad.EmployeeCrud.repositories.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeRepository employeeRepository;

    @Test
    public void testReadAllEmployee() throws Exception {
        List<Employee> mockEmployees = new ArrayList<>();
        mockEmployees.add(new Employee(UUID.randomUUID(), 123456, "John Doe", "123 Main St, City", "HR"));
        Mockito.when(employeeRepository.findAll()).thenReturn(mockEmployees);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/employee/get-all"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("Read all Employee success"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data.length()").value(mockEmployees.size()));
    }

    @Test
    public void testGetEmployeeByNik() throws Exception {
        int nik = 123456;
        Employee mockEmployee = new Employee(UUID.randomUUID(), nik, "John Doe", "123 Main St, City", "HR");
        Mockito.when(employeeRepository.findEmployeeByNik(nik)).thenReturn(mockEmployee);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/employee/nik/{nik}", nik))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("Employee found"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data.id").value(mockEmployee.getId().toString()));
    }

    @Test
    public void testGetEmployeeByName() throws Exception {
        String name = "John";
        List<Employee> mockEmployees = new ArrayList<>();
        mockEmployees.add(new Employee(UUID.randomUUID(), 123456, name, "123 Main St, City", "HR"));
        Mockito.when(employeeRepository.findEmployeeByName(name.toLowerCase())).thenReturn(mockEmployees);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/employee/name/{name}", name))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("Read all Employee with the name of " + name + " success"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.Data.length()").value(mockEmployees.size()));
    }

    @Test
    public void testCreateEmployee() throws Exception {
        CreateEmployeeRequest createRequest = new CreateEmployeeRequest();
        createRequest.setNik(123456);
        createRequest.setName("John Doe");
        createRequest.setAddress("123 Main St, City");
        createRequest.setDepartement("HR");

        Employee mockEmployee = new Employee(UUID.randomUUID(), 123456, "John Doe", "123 Main St, City", "HR");
        Mockito.when(employeeRepository.save(Mockito.any(Employee.class))).thenReturn(mockEmployee);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/employee/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(createRequest)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("Employee has been created"));
    }

    @Test
    public void testUpdateEmployee() throws Exception {
        UUID employeeId = UUID.randomUUID();
        Employee mockEmployee = new Employee(employeeId, 123456, "John Doe", "123 Main St, City", "HR");
        Employee updateDetails = new Employee(employeeId, 123456, "Updated Name", "Updated Address", "Updated Department");

        Mockito.when(employeeRepository.existsById(employeeId)).thenReturn(true);
        Mockito.when(employeeRepository.findById(employeeId)).thenReturn(java.util.Optional.of(mockEmployee));
        Mockito.when(employeeRepository.save(Mockito.any(Employee.class))).thenReturn(mockEmployee);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/employee/update/{id}", employeeId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(updateDetails)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("Employee with the id of " + employeeId + " has been updated"));
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        UUID employeeId = UUID.randomUUID();
        Employee mockEmployee = new Employee(employeeId, 123456, "John Doe", "123 Main St, City", "HR");

        Mockito.when(employeeRepository.existsById(employeeId)).thenReturn(true);
        Mockito.when(employeeRepository.findById(employeeId)).thenReturn(java.util.Optional.of(mockEmployee));

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/employee/delete/{id}", employeeId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.Message").value("Employee with the id of " + employeeId + " has been deleted"));
    }

    // Helper method to convert objects to JSON strings
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
