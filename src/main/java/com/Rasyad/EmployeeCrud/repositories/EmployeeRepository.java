package com.Rasyad.EmployeeCrud.repositories;

import com.Rasyad.EmployeeCrud.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface EmployeeRepository extends JpaRepository<Employee, UUID> {

    @Query(nativeQuery = true, value =
            "SELECT * " +
            "FROM employee Emp " +
            "WHERE Emp.nik = :nik")
    Employee findEmployeeByNik(@Param("nik") int nik);

    @Query(nativeQuery = true, value =
            "SELECT * " +
                    "FROM employee Emp " +
                    "WHERE LOWER(Emp.name) ILIKE CONCAT('%', :name, '%')")
    List<Employee> findEmployeeByName(@Param("name") String name);

}