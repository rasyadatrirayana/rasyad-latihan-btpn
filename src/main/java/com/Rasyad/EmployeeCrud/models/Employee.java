package com.Rasyad.EmployeeCrud.models;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.UUID;

@Entity
@Table(name = "employee", schema = "public")
public class Employee {

    private UUID id;
    private int nik;
    private String name;
    private String address;
    private String departement;

    public Employee(UUID id, int nik, String name, String address, String departement) {
        this.id = id;
        this.nik = nik;
        this.name = name;
        this.address = address;
        this.departement = departement;
    }

    public Employee() {
    }

    @Id
    @GeneratedValue(generator = "uuid_employee")
    @GenericGenerator(name = "uuid_employee", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "Id", unique = true, nullable = false)
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Column(name = "nik", nullable = true)
    public int getNik() {
        return nik;
    }

    public void setNik(int nik) {
        this.nik = nik;
    }

    @Column(name = "name", nullable = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "address", nullable = true)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "departement", nullable = true)
    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }
}
