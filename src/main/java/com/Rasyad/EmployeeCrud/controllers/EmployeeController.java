package com.Rasyad.EmployeeCrud.controllers;


import com.Rasyad.EmployeeCrud.dto.EmployeeDto;
import com.Rasyad.EmployeeCrud.dto.request.CreateEmployeeRequest;
import com.Rasyad.EmployeeCrud.models.Employee;
import com.Rasyad.EmployeeCrud.repositories.EmployeeRepository;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.*;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private EmployeeRepository EmployeeRepository;

    // get all Employee
    @GetMapping("/get-all")
    public ResponseEntity<Object> readAllEmployee(){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        List<Employee> listEmployeeEntity = EmployeeRepository.findAll();
        List<EmployeeDto> listEmployeeDto = new ArrayList<>();

        if(listEmployeeEntity.isEmpty()) {
            result.put("Data", "Employee list is empty");
            responseStatus = HttpStatus.BAD_REQUEST;
        }
        else {

            for (Employee Employee : listEmployeeEntity) {

                EmployeeDto EmployeeDto = modelMapper.map(Employee, EmployeeDto.class);

                listEmployeeDto.add(EmployeeDto);

            }

            result.put("Message", "Read all Employee success");
            result.put("Data", listEmployeeDto);
            result.put("Total", listEmployeeDto.size());
        }

        result.put("Status", responseStatus);
        return new ResponseEntity<Object>(result, responseStatus);

    }

    // get Employee by nik
    @GetMapping("/nik/{nik}")
    public ResponseEntity<Object> getEmployeeByNik(@PathVariable(name = "nik") int nik){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        Employee Employee = EmployeeRepository.findEmployeeByNik(nik);

        if (EmployeeRepository.findEmployeeByNik(nik) != null){
            EmployeeDto EmployeeDto = modelMapper.map(Employee, EmployeeDto.class);
            result.put("Data", EmployeeDto);
            message = "Employee found";
        }
        else {
            message = "Couldn't found Employee with the id of : " + nik;
            responseStatus = HttpStatus.NOT_FOUND;
        }

        result.put("Message", message);
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

    // get Employee by name
    @GetMapping("/name/{name}")
    public ResponseEntity<Object> getEmployeeByName(@PathVariable(name = "name") String name){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        List<Employee> listEmployee = EmployeeRepository.findEmployeeByName(name.toLowerCase());

        List<EmployeeDto> listEmployeeDto = new ArrayList<>();

        if (listEmployee.size() > 0){

            for (Employee Employee : listEmployee) {

                EmployeeDto EmployeeDto = modelMapper.map(Employee, EmployeeDto.class);

                listEmployeeDto.add(EmployeeDto);

            }

            message = "Read all Employee with the name of " + name + " success";
            result.put("Data", listEmployeeDto);
            result.put("Total", listEmployeeDto.size());

        }
        else {
            message = "Couldn't found Employee with the name of : " + name;
            responseStatus = HttpStatus.NOT_FOUND;
        }

        result.put("Message", message);
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

    // create a new Employee
    @PostMapping("/create")
    public ResponseEntity<Object> createEmployee(@Valid @RequestBody CreateEmployeeRequest body){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        Employee employee = modelMapper.map(body, Employee.class);


        EmployeeRepository.save(employee);

        EmployeeDto EmployeeDto = modelMapper.map(body, EmployeeDto.class);

        result.put("Data", body);
        result.put("Message", "Employee has been created");
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

    // update an Employee
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable(name = "id") UUID Id, @Valid @RequestBody Employee EmployeeDetails) {

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        EmployeeDetails.setId(Id);

        if (EmployeeRepository.existsById(Id)){
            message = "Employee with the id of " + Id + " has been updated";

            Employee Employee = EmployeeRepository.findById(Id).get();

            Employee = modelMapper.map(EmployeeDetails, Employee.class);

            EmployeeRepository.save(Employee);
        }
        else {
            message = "Couldn't found Employee with the id of : " + Id;
            responseStatus = HttpStatus.NOT_FOUND;
        }

        result.put("Message", message);
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

    // delete a Employee
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable(name = "id") UUID Id){

        Map<String, Object> result = new HashMap<String, Object>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        Employee Employee = EmployeeRepository.findById(Id).get();

        if (EmployeeRepository.existsById(Id)){
            EmployeeDto EmployeeDto = modelMapper.map(Employee, EmployeeDto.class);
            result.put("Data", EmployeeDto);
            message = "Employee with the id of " + Id + " has been deleted";

            EmployeeRepository.delete(Employee);
        }
        else {
            message = "Couldn't found Employee with the id of : " + Id;
            responseStatus = HttpStatus.NOT_FOUND;
        }

        result.put("Message", message);
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

}
